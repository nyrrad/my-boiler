var bs = require('browser-sync'),
    gulp = require('gulp'),
    gulpIf = require('gulp-if'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    prefixer = require('gulp-autoprefixer'),
    pug = require('gulp-pug'),
    rs = require('run-sequence'),
    sass = require('gulp-sass'),
    usrf = require('gulp-useref'),
    uglify = require('gulp-uglify');

gulp.task('bs', function() {
 bs.init({
    server: {
      baseDir: 'working/dist',
      index: 'index.html' },
    ghostMode: {
      clicks: true,
      forms: true,
      scroll: false
    }
  })
});

gulp.task('pug', function() {
  return gulp.src( 'working/src/pug/**.pug' )
  .pipe(plumber({
    errorHandler: notify.onError('Error: <%= error.message %>')
  }))
  .pipe(pug({
    pretty: true
  }))
  .pipe(gulp.dest('working/dist'))
  .pipe(bs.reload({
    stream: true
  }))
});

gulp.task('sass', function() {
  return gulp.src( 'working/src/sass/**.scss' )
  .pipe(plumber({
    errorHandler: notify.onError('Error: <%= error.message %>')
  }))
  .pipe(sass({outputStyle: 'expanded'}))
  .pipe(prefixer({
    browsers: ['last 2 versions', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], cascade: true
  }))
  .pipe(gulp.dest('working/dist/css'))
  .pipe(bs.reload({
      stream: true
  }))
});

gulp.task('js', function() {
  return gulp.src('working/dist/*.html')
  .pipe(usrf())
  .pipe(gulpIf('*.js', uglify()))
  .pipe(gulp.dest('working/dist'))
  .pipe(bs.reload({
      stream: true
  }))
});

gulp.task('watch', function() {
  gulp.watch('working/src/sass/**/*.scss', ['sass']);
  gulp.watch('working/src/js/*.js', ['js']);
  gulp.watch('working/src/pug/**/*.pug', ['pug']);
});

gulp.task('default', function(callback)  {
  rs('bs', 'pug', 'sass', 'js', 'watch', callback );
});
