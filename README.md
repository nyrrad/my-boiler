
---
**Nyrrad Boiler**  
**v0.1.0**

Darryn Bourgeois  
May 2, 2016  
MIT

---
**Languages Supported**
* Pug
* Sass
* JavaScript
* PHP

---
**Tools / Dependencies**
1. gulp
  * v3.9.1
* gulp-autoprefixer
  * v3.1.0
* gulp-if
  * v2.0.0
* gulp-notify
  * v2.2.0
* gulp-plumber
  * v1.1.0
* gulp-pug
  * v2.0.0
* gulp-sass
  * v2.3.1
* gulp-uglify
  * v1.5.3
* gulp-useref
  * v3.1.0


1. browser-sync
  * v2.12.5
* run-sequence
  * v1.1.5


1. normalize.css
  * v4.1.1
* reset.css
  * v2.0


1. Simple Sass Grid
  * v1.3.2

---

**Usage**  

I currently use Nodejs v6.0.0, Your results may vary

```bash
$ git clone git@gitlab.com:nyrrad/my-boiler.git

$ bash modules.sh

$ gulp
```

This will clone the folder and install all npm modules.  
Feel free to inspect modules.sh, its safe.
